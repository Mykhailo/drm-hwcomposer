#define LOG_TAG "hwc-platform-imagination"

#include "platformimagination.h"
#include <drm/drm_fourcc.h>
#include <log/log.h>
#include <xf86drm.h>

#define MALI_ALIGN(value, base) (((value) + ((base)-1)) & ~((base)-1))

namespace android {

Importer *Importer::CreateInstance(DrmDevice *drm) {
  ImaginationImporter *importer = new ImaginationImporter(drm);
  if (!importer)
    return NULL;

  int ret = importer->Init();
  if (ret) {
    ALOGE("Failed to initialize the hisi importer %d", ret);
    delete importer;
    return NULL;
  }
  return importer;
}

ImaginationImporter::ImaginationImporter(DrmDevice *drm)
    : DrmGenericImporter(drm), drm_(drm) {
}

ImaginationImporter::~ImaginationImporter() {
}

int ImaginationImporter::Init() {
  int ret = hw_get_module(GRALLOC_HARDWARE_MODULE_ID,
                          (const hw_module_t **)&gralloc_);
  if (ret) {
    ALOGE("Failed to open gralloc module %d", ret);
    return ret;
  }

  if (strcasecmp(gralloc_->common.author, "Imagination Technologies"))
    ALOGW("Using non-Imagination gralloc module: %s/%s\n",
          gralloc_->common.name, gralloc_->common.author);

  return 0;
}

int ImaginationImporter::ImportBuffer(buffer_handle_t handle,
                                      hwc_drm_bo_t *bo) {
  IMG_native_handle_t *hnd = (IMG_native_handle_t *)handle;
  if (!hnd)
    return -EINVAL;

  uint32_t gem_handle;
  int ret = drmPrimeFDToHandle(drm_->fd(), hnd->fd[0], &gem_handle);
  if (ret) {
    ALOGE("failed to import prime fd %d ret=%d", hnd->fd[0], ret);
    return ret;
  }

  memset(bo, 0, sizeof(hwc_drm_bo_t));
  bo->width = hnd->iWidth;
  bo->height = hnd->iHeight;
  bo->usage = hnd->usage;
  bo->format = ConvertHalFormatToDrm(hnd->iFormat);
  bo->gem_handles[0] = gem_handle;
  bo->pitches[0] = ALIGN(hnd->iWidth, HW_ALIGN) * hnd->uiBpp >> 3;
  bo->offsets[0] = 0;

  ret = drmModeAddFB2(drm_->fd(), bo->width, bo->height, bo->format,
                      bo->gem_handles, bo->pitches, bo->offsets, &bo->fb_id, 0);
  if (ret) {
    ALOGE("could not create drm fb %d", ret);
    return ret;
  }

  return ret;
}

bool ImaginationImporter::CanImportBuffer(buffer_handle_t handle) {
  IMG_native_handle_t const
      *hnd = reinterpret_cast<IMG_native_handle_t const *>(handle);

  return hnd && (hnd->usage & GRALLOC_USAGE_HW_FB);
}

uint32_t ImaginationImporter::ConvertHalFormatToDrm(uint32_t hal_format) {
  uint32_t dss_format = HAL_PIXEL_FORMAT_RGBA_8888;

  /* convert color format */
  switch (hal_format) {
    case HAL_PIXEL_FORMAT_RGBA_8888:
    case HAL_PIXEL_FORMAT_BGRA_8888:
      dss_format = DRM_FORMAT_ARGB8888;
      break;

    case HAL_PIXEL_FORMAT_RGBX_8888:
    case HAL_PIXEL_FORMAT_BGRX_8888:
      dss_format = DRM_FORMAT_XRGB8888;
      break;

    case HAL_PIXEL_FORMAT_RGB_565:
      dss_format = DRM_FORMAT_RGB565;
      break;

    case HAL_PIXEL_FORMAT_TI_NV12:
    case HAL_PIXEL_FORMAT_TI_NV12_1D:
    case HAL_PIXEL_FORMAT_NV12:
      dss_format = DRM_FORMAT_NV12;
      break;

    default:
      /* Should have been filtered out */
      ALOGV("Unsupported pixel format");
  }

  return dss_format;
}

class PlanStageImagination : public Planner::PlanStage {
 public:
  int ProvisionPlanes(std::vector<DrmCompositionPlane> *composition,
                      std::map<size_t, DrmHwcLayer *> &layers, DrmCrtc *crtc,
                      std::vector<DrmPlane *> *planes) {
    int layers_added = 0;
    // Fill up as many DRM planes as we can with buffers that have HW_FB usage.
    // Buffers without HW_FB should have been filtered out with
    // CanImportBuffer(), if we meet one here, just skip it.
    for (auto i = layers.begin(); i != layers.end(); i = layers.erase(i)) {
      if (!(i->second->gralloc_buffer_usage & GRALLOC_USAGE_HW_FB))
        continue;

      int ret = Emplace(composition, planes, DrmCompositionPlane::Type::kLayer,
                        crtc, std::make_pair(i->first, i->second));
      layers_added++;
      // We don't have any planes left
      if (ret == -ENOENT)
        break;
      else if (ret) {
        ALOGE("Failed to emplace layer %zu, dropping it", i->first);
        return ret;
      }
    }
    // If we didn't emplace anything, return an error to ensure we force client
    // compositing.
    if (!layers_added)
      return -EINVAL;

    return 0;
  }
};

std::unique_ptr<Planner> Planner::CreateInstance(DrmDevice *) {
  std::unique_ptr<Planner> planner(new Planner);
  planner->AddStage<PlanStageImagination>();
  return planner;
}
}  // namespace android

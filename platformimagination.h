#ifndef PLATFORMIMAGINATION_H
#define PLATFORMIMAGINATION_H
#include "hal_public.h"

#include "drmdevice.h"
#include "platform.h"
#include "platformdrmgeneric.h"

#include <stdatomic.h>

#include <hardware/gralloc.h>

namespace android {

class ImaginationImporter : public DrmGenericImporter {
 public:
  ImaginationImporter(DrmDevice *drm);
  ~ImaginationImporter() override;

  int Init();

  int ImportBuffer(buffer_handle_t handle, hwc_drm_bo_t *bo) override;

  bool CanImportBuffer(buffer_handle_t handle) override;

  uint32_t ConvertHalFormatToDrm(uint32_t hal_format);

 private:
  DrmDevice *drm_;

  const gralloc_module_t *gralloc_;
};
}  // namespace android

#endif  // PLATFORMIMAGINATION_H
